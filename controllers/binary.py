import logging
import json
import base64
from odoo import SUPERUSER_ID, _, http
from odoo.addons.web.controllers.main import Binary
from odoo.http import request
_logger = logging.getLogger(__name__)

class BinaryCustom(Binary):
    @http.route('/web/binary/upload_attachment', type='http', auth="user")
    def upload_attachment(self, callback, model, id, ufile, parent_id=False):
        Model = request.env['ir.attachment']
        out = """<script language="javascript" type="text/javascript">
                            var win = window.top.window;
                            win.jQuery(win).trigger(%s, %s);
                        </script>"""
        try:
            post = {
                'name': ufile.filename,
                'datas': base64.encodestring(ufile.read()),
                'datas_fname': ufile.filename,
                'is_main_version': False,
            }
            if parent_id:
                parent = Model.search([('id', '=', int(parent_id))])
                if parent.parent_id:
                    if model in ['product.template', 'product.product']:
                        request.env['ir.attachment']. \
                            search([
                            ('res_id', '=', parent.res_id),
                            '|',
                            ('id', '=', parent_id),
                            ('parent_id', '=', parent_id)
                        ]).write({'is_main_version': False})
                        post.update(
                            {
                                'res_model': parent.res_model,
                                'res_id': int(parent.res_id),
                                'parent_id': int(parent.parent_id),
                                'is_main_version': True
                            }
                        )
                if model == 'res.partner':
                    request.env['ir.attachment'].\
                        search([
                            ('res_id', '=', parent.res_id),
                            '|',
                            ('id', '=', parent_id),
                            ('parent_id', '=', parent_id)
                        ]).write({'is_main_version': False})
                    post.update({
                        'res_model': parent.res_model,
                        'res_id': int(parent.res_id),
                        'is_main_version': True
                    })

                attachment = Model.create(post)
            else:
                if model == 'product.product':
                    product = request.env['product.product'].browse(int(id))
                    print product.product_tmpl_id.id
                    _logger.exception(
                        "product tmeplate id %s" % product.product_tmpl_id.id)
                    post.update({
                        'res_model': 'product.template',
                        'res_id': int(product.product_tmpl_id.id),
                        'is_main_version': True
                    })
                else:
                    post.update({
                        'res_model': model,
                        'res_id': int(id),
                        'is_main_version': True
                    })
                attachment = Model.create(post)
                attachment.write({'parent_id': attachment.id})
            args = {
                'filename': ufile.filename,
                'mimetype': ufile.content_type,
                'id': attachment.id,
            }
        except Exception:
            args = {'error': _("Something horrible happened")}
            _logger.exception("Fail to upload attachment %s" % ufile.filename)
        return out % (json.dumps(callback), json.dumps(args))