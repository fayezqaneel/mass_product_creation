odoo.define('mass_product_create.create_button', function(require) {

    "use strict";

    var core = require('web.core');
    var KanbanView = require('web_kanban.KanbanView');
    var Pager = require('web.Pager');
    var FormView = require('web.FormView');
    var form_common = require('web.form_common');
    var sidebar = require('web.Sidebar');
    var data = require('web.data');
    var framework = require('web.framework');
    var Dialog = require('web.Dialog');
    var _t = core._t;
    var QWeb = core.qweb;
    var Model = require('web.Model');
    var FieldOne2Many = core.form_widget_registry.get('one2many');
    var base = require('web_editor.base');
    var FieldBinaryFile = core.form_widget_registry.get('binary');
    var imageFile = core.form_widget_registry.get('image');


    var ListView = require('web.ListView');
    var NotificationManager = require('web.notification').NotificationManager;


    FieldBinaryFile.include({
        init: function (){
           this._super.apply(this, arguments);
           this.max_upload_size = 10000 * 1024 * 1024;
        }
    });

    imageFile.include({
        init: function (){
           this._super.apply(this, arguments);
           this.max_upload_size = 10000 * 1024 * 1024;
        }
    });

    sidebar.include({
        redraw: function(){
            this._super.apply(this, arguments);
            this.$el.find('.o_dropdown').eq(1).hide();
        },
        on_attachment_changed: function (event) {
            event.preventDefault();
            event.stopPropagation();
            var self = this;
            var self = this;
            var form = $(this);
            var callback = $("input[name=callback]").val();
            var model = $("input[name=model]").val();
            var id = $("input[name=id]").val();
            var csrf_token = $("input[name=csrf_token]").val();
            var parent_id = $("input[name=parent_id]").val();

            var $target = $(event.target);
            var retmsg = '';
            if ($target.val() !== '') {
                var cont =0;
                var notification_manager = new NotificationManager(self);
                notification_manager.appendTo($('body'));
                notification_manager.notify(
                    _t('Upload Started'),
                    _t('Uploading ' + $target[0].files.length + ' file(s)...'),
                    true
                );
                _.each($target[0].files, function(file){
                    var querydata = new FormData();
                    querydata.append('callback', callback);
                    querydata.append('ufile',file);
                    querydata.append('model', model);
                    querydata.append('id', id);
                    querydata.append('csrf_token', csrf_token);
                    if(parent_id && typeof parent_id !='undefined'){
                        querydata.append('parent_id', parent_id);
                    }

                    var ret = $.ajax({
                        url: '/web/binary/upload_attachment',
                        type: 'POST',
                        data: querydata,
                        cache: false,
                        processData: false,
                        contentType: false,
                        success: function(msg) {
                            cont+=1;
                            if(cont==$target[0].files.length)
                            {
                                $('.o_notification_title').text('Upload Completed');
                                $('.o_notification_content').text($target[0].files.length+' file(s) uploaded');
                                $('.o_notification_manager .o_notification').css('background', '#dcf3e7');
                                if(self.getParent().fields.model_attachments){
                                    self.getParent().fields.model_attachments.view.reload();
                                    self.getParent().fields.model_attachments.view.reload();
                                }
                                if(self.getParent().fields.files_ids){
                                    self.getParent().fields.files_ids.view.reload();
                                    self.getParent().fields.files_ids.view.reload();
                                }
                            }
                        }
                    });
                });
            }
        },
        on_attachment_delete: function(e) {
           e.preventDefault();
            e.stopPropagation();
            var self = this;
            var $e = $(e.currentTarget);
            var options = {
                confirm_callback: function () {
                    new data.DataSet(this, 'ir.attachment')
                        .unlink([parseInt($e.attr('data-id'), 10)])
                        .done(function() {
                            self.do_attachement_update(self.dataset, self.model_id);
                            if(self.getParent().fields.model_attachments){
                                self.getParent().fields.model_attachments.view.reload();
                            }

                            if(self.getParent().fields.files_ids){
                                self.getParent().fields.files_ids.view.reload();
                            }
                        });
                }
            };
            Dialog.confirm(this, _t("Do you really want to delete this attachment ?"), options);
        },
    });


    KanbanView.include({
        render_buttons: function($node) {

            this._super.apply(this, arguments);

            if (this.$buttons) {
                if (this.model == 'product.template') {
                    this.$buttons.append('<button class="btn btn-default btn-sm o_create_mass_product_button" type="button">Mass Create</button>')
                    let new_button = this.$buttons.find('.o_create_mass_product_button');
                    new_button && new_button.click(this.proxy('create_mass_product_button'));
                }
            }

        },

        create_mass_product_button: function() {
            var self = this;
            this.do_action({
                type: 'ir.actions.act_window',
                res_model: 'mass.product.creation',
                res_id: false,
                views: [
                    [false, 'form']
                ],
                target: 'current',
                on_reverse_breadcrumb: function() {
                    return self.reload();
                }
            });
        }

    });

    ListView.include({
        start: function(){
            this._super.apply(this, arguments);
            var self = this;
            if(self.model=='product.product'){

                $('body').on("search", function(){
                    if(self.dataset && self.dataset.parent_view && self.dataset.parent_view.datarecord &&self.dataset.parent_view.datarecord.id){
                        self.oldDomain  = [["customer_id", "=", self.dataset.parent_view.datarecord.id]]
                        self.customer_id = self.dataset.parent_view.datarecord.id;
                    }

                    if($('#search-input').val()){
                        window.x2m = self;
                        self._limit = 10000;
                        var res = self.do_search([["customer_id", "=", self.customer_id],'|',["default_code", "ilike", $('#search-input').val()], ["name", "ilike", $('#search-input').val()]], false, []);
                        self.pager.do_hide();
                    }
                });

                $('#search-input').on('change',function(){
                    $('body').trigger("search");
                });
            }
        },
        load_list: function () {
            var res = this._super.apply(this, arguments);

            var self = this;
            if(self.model=='ir.attachment'){
                self.$el.find('td[data-field="download_link"]').each(function(){
                    var link = $(this).text().trim();
                    if(link!=''){
                        $(this).html("<a href='"+link+"' target='_blank'>Download</a>");
                    }
                });

                self.$el.find('td[data-field="upload_new_version"]').each(function(){
                    $(this).html("<a href='#' class='upload-new-version' target='_blank'>Upload</a>");
                    $(this).find('a.upload-new-version').click(self.handle_upload_version);
                });

                self.$el.find('td[data-field="write_uid"]').each(function(){
                    $(this).html("<a href='#' class='show_user_profile' target='_blank'>"+$(this).text()+"</a>");
                    $(this).find('a.show_user_profile').click(function(e){
                        self.handle_show_user(e, self)
                    });
                });


                self.$el.find('td[data-field="recover_link"]').each(function(){
                    if($('.breadcrumb li.active').text() !== 'Archived Files'){
                        $(this).html("<a href='#' class='recover-version' target='_blank'>Recover</a>");
                        $(this).find('a.recover-version').click(function(e){
                            self.handle_recover_version(e, self)
                        });
                     }else {
                        $(this).html("<span></span>");
                     }
                });



                self.$el.find('tr').each(function(){
                    var id = $(this).data('id');
                    if(id && self.records._byId[id].attributes && self.records._byId[id].attributes.parent_id){
                        $(this).attr('rel', self.records._byId[id].attributes.parent_id[0]);
                    }
                });

            }
            if(self.model=='product.product'){
                self.$el.find('td[data-field="files_col"]').each(function(){
                    $(this).html(QWeb.render('viewFilesDropdown'));
                    $(this).find('.drop').off('click').on('click', function(e){
                        $(this).parents('tr').siblings().find('.files-area .links').removeClass('show');
                        $(this).next().toggleClass('show');
                        e.stopPropagation();
                    });
                    $(this).find('.download-proof-btn-one').on('click', function(e){
                        e.preventDefault();
                        $(e.target).parents('tr').find('input').not(':checked').trigger('click');
                        $(e.target).parents('tr').siblings().find('input:checked').trigger('click');
                        $('.download-proof-btn').trigger('click');
                    });
                    $(this).find('.download-design-btn-one').on('click', function(e){
                        e.preventDefault();
                        $(e.target).parents('tr').find('input').not(':checked').trigger('click');
                        $(e.target).parents('tr').siblings().find('input:checked').trigger('click');
                        $('.download-design-btn').trigger('click');
                    });
                    $(this).find('.show_files').on('click', function(e){
                        e.preventDefault();
                        var selected_ids = [$(e.target).parents('tr:first').data('id')];
                         var model_obj = new Model('product.product');
                            //you can hardcode model name as: new Model("module.model_name");
                            //you can change the function name below
                            /* you can use the python function to get the IDS
                                  @api.multi
                                def bulk_verify(self):
                                        for record in self:
                                            print record
                            */
                            model_obj.call('show_product_template', [selected_ids], {
                                context: self.dataset.context
                            })
                            .then(function(result) {
                                self.do_action(result);
                            });
                    });
                    window.x2m = self;
                });
            }

            return res;
        },
        do_search: function (domain, context, group_by) {
           this._super.apply(this, arguments);

        },
        handle_show_user: function(e, self){
            e.preventDefault();

            var model_obj = new Model('ir.attachment');
            model_obj.call('show_user', [$(e.target).parents('tr:first').data('id')], {
                    context: self.dataset.context
                })
                .then(function(result) {
//                    self.do_action(result);
                    self.do_action({
                        type: 'ir.actions.act_window',
                        res_model: 'res.users',
                        res_id: result,
                        views: [[false, 'form']],
                        target: 'current',
                    });
                });

        },
        handle_recover_version: function(e, self){
            e.preventDefault();
            var id = $(e.target).parents('tr:first').data('id');
            var model_obj = new Model('ir.attachment');
            model_obj.call('recover', [id], {})
            .then(function(result) {
                self.do_action({'type': 'ir.actions.client', 'tag': 'history_back'});
            });
        },
        handle_upload_version: function(e){
           e.preventDefault();
           var id = $(e.target).parents('tr').data('id');
           if($('.o_hidden_input_file:first input#parent_id').length == 0){
                console.log($('.o_hidden_input_file form'));
                $('.o_hidden_input_file:first form').append("<input id='parent_id' type='hidden' name='parent_id' value='"+id+"' />");
           }else{
                $('.o_hidden_input_file:first input#parent_id').val(id);
           }
           $('.o_form_input_file:first').removeAttr('multiple').trigger('click');
        }
    });
    FormView.include({
        toggle_buttons: function() {
            $('body').trigger('mode_changed');
            this._super.apply(this, arguments);
            if (this.$buttons && this.get("actual_mode") == 'create') {
                if (this.model == 'mass.product.creation' && this.$buttons.find('.o_form_buttons_edit button:contains("Save & Create Quote")').length < 1) {
                    this.$buttons.find('.o_form_buttons_edit').append('<button class="btn btn-primary btn-sm o_create_mass_product_button" type="button">Save & Create Quote</button>')
                    let new_button = this.$buttons.find('.o_create_mass_product_button');
                    new_button && new_button.click(this.proxy('save_create_button'));
                }
            } else {
                if(this.$buttons){
                    this.$buttons.find('.o_form_buttons_edit button:contains("Save & Create Quote")').remove();
                }
            }
        },
        save_create_button: function() {
            $('.o_form_button_save').trigger('click');

            var self = this;
            core.bus.on('form_view_saved', this, function() {
                if(window.location.href.indexOf("model=mass.product.creation")!==-1){
                    var model_obj = new Model('mass.product.creation.product.line');
                    model_obj.call('mass_create', [self.datarecord.product_line_ids], {
                            context: self.dataset.context
                        })
                        .then(function(result) {
                            self.do_action(result);
                        });
                }
            });
        }

    });

base.ready().always(function () {
    QWeb.add_template('/mass_product_creation/static/src/xml/list_view.xml');
});

    var One2ManySelectable = FieldOne2Many.extend({
        // my custom template for unique char field
        template: 'One2ManySelectable',

        multi_selection: true,
        //button click
        events: {
            "click .cf_button_confirm": "action_selected_lines",
            "click .delete-products-btn": "action_selected_lines_delete",
            "click .download-proof-btn": "action_download_proof",
            "click .download-design-btn": "action_download_design"
        },
        start: function() {
            this._super.apply(this, arguments);
            var self = this;
        },
        action_selected_lines_delete: function() {
            var self = this;
            var selected_ids = self.get_selected_ids_one2many();
            if (selected_ids.length === 0) {
                this.do_warn(_t("You must choose at least one record."));
                return false;
            }
            var model_obj = new Model(this.dataset.model);
            //you can hardcode model name as: new Model("module.model_name");
            //you can change the function name below
            /* you can use the python function to get the IDS
                  @api.multi
                def bulk_verify(self):
                        for record in self:
                            print record
            */
            model_obj.call('unlink', [selected_ids], {
                    context: self.dataset.context
                })
                .then(function(result) {
                    console.log(result);
                    self.do_action(result);
                });
        },
        //passing ids to function
        action_selected_lines: function() {

            var self = this;
            var selected_ids = self.get_selected_ids_one2many();
            if (selected_ids.length === 0) {
                this.do_warn(_t("You must choose at least one record."));
                return false;
            }
            var model_obj = new Model(this.dataset.model);
            //you can hardcode model name as: new Model("module.model_name");
            //you can change the function name below
            /* you can use the python function to get the IDS
                  @api.multi
                def bulk_verify(self):
                        for record in self:
                            print record
            */
            model_obj.call('mass_create_customer', [selected_ids], {
                    context: self.dataset.context
                })
                .then(function(result) {
                    console.log(result);
                    self.do_action(result);
                });
        },
        action_download_proof: function() {

            var self = this;
            var selected_ids = self.get_selected_ids_one2many();
            if (selected_ids.length === 0) {
                this.do_warn(_t("You must choose at least one record."));
                return false;
            }
            if (selected_ids.length > 1) {
                this.do_warn(_t("You must choose at maximum one record."));
                return false;
            }
            var model_obj = new Model(this.dataset.model);
            //you can hardcode model name as: new Model("module.model_name");
            //you can change the function name below
            /* you can use the python function to get the IDS
                  @api.multi
                def bulk_verify(self):
                        for record in self:
                            print record
            */
            model_obj.call('download_proof', [selected_ids], {
                    context: self.dataset.context
                })
                .then(function(result) {
                    if(result){
                        var a = document.createElement('a');
                        a.href = result;
                        a.download = result.split('/').pop();
                        a.click();
                    }else{
                        self.do_warn(_t("No proof file found for this product."));
                    }
                });
        },
        action_download_design: function() {

            var self = this;
            var selected_ids = self.get_selected_ids_one2many();
            if (selected_ids.length === 0) {
                this.do_warn(_t("You must choose at least one record."));
                return false;
            }
            if (selected_ids.length > 1) {
                this.do_warn(_t("You must choose at maximum one record."));
                return false;
            }
            var model_obj = new Model(this.dataset.model);
            //you can hardcode model name as: new Model("module.model_name");
            //you can change the function name below
            /* you can use the python function to get the IDS
                  @api.multi
                def bulk_verify(self):
                        for record in self:
                            print record
            */
            model_obj.call('download_design', [selected_ids], {
                    context: self.dataset.context
                })
                .then(function(result) {
                    if(result){
                        var a = document.createElement('a');
                        a.href = result;
                        a.download = result.split('/').pop();
                        a.click();
                    }else{
                        self.do_warn(_t("No print file found for this product."));
                    }
                });
        },
        //collecting the selected IDS from one2manay list
        get_selected_ids_one2many: function() {
            var ids = [];
            this.$el.find('td.o_list_record_selector input:checked')
                .closest('tr').each(function() {

                    ids.push(parseInt($(this).context.dataset.id));
                    console.log(ids);
                });
            return ids;
        },


    });
    // register unique widget, because Odoo does not know anything about it
    //you can use <field name="One2many_ids" widget="x2many_selectable"> for call this widget

    var One2ManySelectableAttachment = FieldOne2Many.extend({
        // my custom template for unique char field
        template: 'One2ManySelectableAttachment',

        multi_selection: true,
        //button click
        events: {
            "click .archived-files-btn": "archived_files",
            "click .upload-btn": "general_upload",
            "click .archive-files-btn": "archive_multiple_files",
            "click .recover-version-btn": "recover_version_btn"

        },
        start: function() {
            this._super.apply(this, arguments);
            var self = this;
        },
        recover_version_btn: function(){
            var self = this;
            var id = 0;
            if(self.field_manager && self.field_manager.datarecord && self.field_manager.datarecord.id){
                id = self.field_manager.datarecord.id;
            }
            var selected_ids = self.get_selected_ids_one2many();
            if (selected_ids.length === 0) {
                this.do_warn(_t("You must choose at least one record."));
                return false;
            }
            if (selected_ids.length > 1) {
                this.do_warn(_t("You must choose at maximum one record."));
                return false;
            }
            var parent = self.$el.find('tr[data-id="'+selected_ids[0]+'"]').attr('rel');

            this.do_action({
                name: _t('Recover Version'),
                type: 'ir.actions.act_window',
                res_model: "ir.attachment",
                views: [[false, 'list'], [false, 'form']],
                view_type : "list",
                view_mode : "list",
                domain: [ ['is_main_version', '=', false], '|', ['parent_id', '=', parseInt(parent)], ['id', '=', parseInt(parent)]],
            });
        },
        archived_files: function() {
            var self = this;
            var id = 0;
            if(self.field_manager && self.field_manager.datarecord && self.field_manager.datarecord.id){
                id = self.field_manager.datarecord.id;
            }
            this.do_action({
                name: _t('Archived Files'),
                type: 'ir.actions.act_window',
                res_model: "ir.attachment",
                views: [[false, 'list'], [false, 'form']],
                view_type : "list",
                view_mode : "list",
                domain: [['archive', '=', '0'], ['res_id', '=', id]],
            });
        },
        archive_multiple_files: function(){
            var self = this;
            var selected_ids = self.get_selected_ids_one2many();
            if (selected_ids.length === 0) {
                this.do_warn(_t("You must choose at least one record."));
                return false;
            }
            var model_obj = new Model(this.dataset.model);
            //you can hardcode model name as: new Model("module.model_name");
            //you can change the function name below
            /* you can use the python function to get the IDS
                  @api.multi
                def bulk_verify(self):
                        for record in self:
                            print record
            */
            model_obj.call('archive_files', [selected_ids], {
                context: self.dataset.context
            })
            .then(function(result) {
                self.view.recursive_reload();
            });
        },
        general_upload: function(){
            $('.o_form_input_file input#parent_id').remove();
            $('.o_form_input_file:first').attr('multiple', 'multiple');
            $('.o_form_input_file:first').trigger('click');
        },
        //collecting the selected IDS from one2manay list
        get_selected_ids_one2many: function() {
            var ids = [];
            this.$el.find('td.o_list_record_selector input:checked')
                .closest('tr').each(function() {

                    ids.push(parseInt($(this).context.dataset.id));
                    console.log(ids);
                });
            return ids;
        },


    });
    var One2ManySelectableAttachmentCustomer = FieldOne2Many.extend({
        // my custom template for unique char field
        template: 'One2ManySelectableAttachmentCustomer',

        multi_selection: true,
        //button click
        events: {
            "click .recover-version-btn": "recover_version_btn",
            "click .delete-files-btn": "action_selected_lines_delete",
        },
        load_views: function() {
            var self = this;
            var res =  this._super.apply(this, arguments);
            $.when(res).then(function(){

                console.log(self);
                var model_obj = new Model('product.product');
                 model_obj.call('get_tabs', [self.view.datarecord.product_ids], {})
                 .then(function(result) {
                      var tags = result.map(function(i){
                            return '<a  class="tag-holder">'+i+'</a>'
                      });
                      $('.tabs-area').append(tags.join(" "));
                      $('.tabs-area').on('click', 'a', function(e){
                        self.filter_results(e);
                      })
                 });
            });
            return res;

        },
        filter_results: function(e){
            e.preventDefault();
            $(e.target).addClass('active').siblings().removeClass('active');
            var tag = $(e.target).text();
            if ($(e.target).attr('rel') == '' ) {
                this.getChildren().pop().getChildren().pop().do_search([["id", "in", []], ['mimetype', 'in', ['application/cdr', 'application/coreldraw', 'application/x-cdr', 'application/x-coreldraw', 'image/cdr', 'image/x-cdr', 'zz-application/zz-winassoc-cdr', 'image/x-coreldraw']]], false, []);
                return;
            }
            var ids = $(e.target).attr('rel').split(',');
            window.x2m = this;
            if(ids){
                this.getChildren().pop().getChildren().pop().do_search([["id", "in", ids], ['mimetype', 'in', ['application/cdr', 'application/coreldraw', 'application/x-cdr', 'application/x-coreldraw', 'image/cdr', 'image/x-cdr', 'zz-application/zz-winassoc-cdr', 'image/x-coreldraw']]], false, []);
            }
            $('.tags-pagination .o_x2m_control_panel').css('margin-top', '0px');
        },
        action_selected_lines_delete: function() {
            var self = this;
            var selected_ids = self.get_selected_ids_one2many();
            if (selected_ids.length === 0) {
                this.do_warn(_t("You must choose at least one record."));
                return false;
            }
            var model_obj = new Model('ir.attachment');
            //you can hardcode model name as: new Model("module.model_name");
            //you can change the function name below
            /* you can use the python function to get the IDS
                  @api.multi
                def bulk_verify(self):
                        for record in self:
                            print record
            */
            model_obj.call('unlink', [selected_ids], {})
                .then(function(result) {
                    console.log(result);
                    self.do_action(result);
                });
        },
        recover_version_btn: function(){
            var self = this;
            var id = 0;
            if(self.field_manager && self.field_manager.datarecord && self.field_manager.datarecord.id){
                id = self.field_manager.datarecord.id;
            }
            var selected_ids = self.get_selected_ids_one2many();
            if (selected_ids.length === 0) {
                this.do_warn(_t("You must choose at least one record."));
                return false;
            }
            if (selected_ids.length > 1) {
                this.do_warn(_t("You must choose at maximum one record."));
                return false;
            }
            var parent = self.$el.find('tr[data-id="'+selected_ids[0]+'"]').attr('rel');

            this.do_action({
                name: _t('Recover Version'),
                type: 'ir.actions.act_window',
                res_model: "ir.attachment",
                views: [[false, 'list'], [false, 'form']],
                view_type : "list",
                view_mode : "list",
                domain: [ ['is_main_version', '=', false], '|', ['parent_id', '=', parseInt(parent)], ['id', '=', parseInt(parent)]],
            });
        },

        //collecting the selected IDS from one2manay list
        get_selected_ids_one2many: function() {
            var ids = [];
            this.$el.find('td.o_list_record_selector input:checked')
                .closest('tr').each(function() {

                    ids.push(parseInt($(this).context.dataset.id));
                    console.log(ids);
                });
            return ids;
        },


    });
    core.form_widget_registry.add('one2many_selectable_attachment', One2ManySelectableAttachment);
    core.form_widget_registry.add('one2many_selectable_attachment_customer', One2ManySelectableAttachmentCustomer);
    core.form_widget_registry.add('one2many_selectable', One2ManySelectable);

})